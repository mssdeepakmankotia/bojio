<?php get_header(); ?>
    <!-- Side nav -->
    <nav class="side-nav" role="navigation">

      <ul class="nav-side-nav">
        <li><a class="tooltip-side-nav" href="#section-1" title="" data-original-title="Owesome Activities" data-placement="left"></a></li>
        <li><a class="tooltip-side-nav" href="#section-2" title="" data-original-title="How it Works" data-placement="left"></a></li>
        <li><a class="tooltip-side-nav" href="#section-3" title="" data-original-title="Terms & Privacy" data-placement="left"></a></li>
        <li><a class="tooltip-side-nav" href="#to-top" title="" data-original-title="Back" data-placement="left"></a></li>
      </ul>
      
    </nav> <!-- /.side-nav -->




    <!-- Jumbotron -->
    <header class="jumbotron" role="banner" style="background:url('<?php echo get_option('header_image'); ?>') no-repeat scroll 0 0 / cover  #2E2F41">

      <div class="container">

        <div class="row">

          <div class="col-md-6" >

            <!-- Logo -->
            <figure class="text-center logo_row">
              <a href="./index.html">
               <!--  <img class="img-logo" src="images/logo2.png" alt="">-->
              </a>
            </figure> <!-- /.text-center -->

            <!-- Title -->
            <h1 data-anijs="if: animationend, on: body, do: swing animated"><?php echo get_option('header_title'); ?></h1>

            <!-- Sub title -->
            <p><?php echo get_option('header_tag_line'); ?></p>

            <!-- Button -->
            <p class="btn-app-store">
              <a class="btn btn-danger btn-lg first-iframe hinge animated" href="<?php echo get_option('google_play_url'); ?>"  data-anijs="if: animationend, on: .splash-head, do: hinge">
                <img src="<?php bloginfo('template_directory') ?>/images/btn-google-store.png" alt="" >
              </a>
			   <a class="btn btn-danger btn-lg first-iframe hinge animated" href="<?php echo get_option('app_store'); ?>"  data-anijs="if: animationend, on: .splash-head, do: hinge">
                <img src="<?php bloginfo('template_directory') ?>/images/btn-app-store.png" alt="">
              </a>
            </p> <!-- /.btn-app-store -->

          </div> <!-- /.col-md-6 -->

          <div class="col-md-6  " >

            <!-- Images showcase -->
            <figure>
              <img class="img-iPhone" src="<?php bloginfo('template_directory') ?>/images/phone_img.png" alt="">
            </figure>

          </div> <!-- /.col-md-6 -->
          
        </div> <!-- /.row -->
        
      </div> <!-- /.container -->

    </header> <!-- /.jumbotron -->

	

    <!-- Services -->
    <section class="services-section" id="section-1" >

      <div class="container">

        <div class="row">

          <div class="col-md-12 col-services animBlock floatl notViewed" data-position="left">
            
          <div class="activities_row ">
		
			
			<?php
				$page_id = 11;
				$page_data = get_page( $page_id ); 

				$content = $page_data->post_content; // Get Content
				$title = $page_data->post_title; // Get title
			?>


            <!-- Title -->
            <h2><?php echo $title ?> <strong>Activities</strong></h2>
             <p class="line_row"><span><i class="fa fa-mobile-phone"></i></span></p>
            <!-- Description -->
		<?php echo $content ?>
		
        </div>
		
          </div> <!-- /.col-md-4 -->

       
        </div> <!-- /.row -->
        
      </div> <!-- /.container -->
      
    </section> <!-- /.services-section -->
<!-- Features -->
    <section class="features-section" id="section-2">
<div class="container">
 <div class="row">
<h2><strong>How it</strong> Works</h2>
					   <p class="line_row"><span><i class="fa fa-mobile-phone"></i></span></p>
</div>
<?php
		 $query = new WP_Query( array('post_type' => 'events', 'posts_per_page' => 5,'order'=>'ASC' ) );
		 
			if($query->have_posts()){
					$i=1;
					
				 while ( $query->have_posts() ) : $query->the_post(); ?>
				<?php if($i%2!=0){?>
			<!--events part ends-->

 				 <div class="row">
				    
					  <div class="activities_row " >
				  			
				    <div class="col-md-7 col-features text-center" data-position="left">
				    
					<!-- Images showcase -->
					<figure>
						<img title="img-iPhone" alt="thumb image" class="wp-post-image" 
             src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>" style="width:320px; height:auto;">
				
					
					</figure>

				    </div> <!-- /.col-md-5 -->

				    <div class="col-md-5 col-features features-content" data-position="right">
				
					<!-- Title -->
					<h3 id="title-1"><span class="circle"><?php echo $i; $i++;?></span> <?php the_title(); ?></h3>

					<!-- Description -->
					<p><?php the_content(); ?></p>

					<p>
					  <a class="btn btn-danger" href="#title-2">Learn more</a>
					</p>

				    </div> <!-- /.col-md-7 -->
				    
					 </div> 
				  </div> <!-- /.row -->
<?php } else { ?>





        <div class="row media-screen-800">
         
		 <div class="activities_row">
          
		  <div class="col-md-7 col-features features-content animBlock floatl notViewed" data-position="left">
            
            <!-- Title -->
            <h3 id="title-2"><span class="circle"><?php echo $i; $i++;?></span>  <?php the_title(); ?></h3>

            <!-- Description -->
           <p><?php the_content(); ?></p>
            <p>
              <a class="btn btn-danger" href="#title-2">Learn more</a>
            </p>

          </div> <!-- /.col-md-7 -->

          <div class="col-md-5 col-features text-center notViewed animBlock floatr" data-position="right">
            
            <!-- Images showcase -->
            <figure>
              <img title="img-iPhone" alt="thumb image" class="wp-post-image" 
             src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>" style="width:320px; height:auto;">
            </figure>

          </div> <!-- /.col-md-5 -->
          </div>
        </div> <!-- /.row -->



		<?php }?>
		<!--events part ends-->		
	
		<?php  
			wp_reset_postdata(); ?>
			<?php endwhile; ?>
		<?php } else {echo "No Event Added";}?>	
	
		
      </div> <!-- /.container -->

    </section> <!-- /.features-section -->

		
		
	<!--footer section -->
   <?php get_footer();?>
	<!--footer section ends -->

   	<script src="http://anijs.github.io/lib/anijs/anijs.js"></script>
   <script src="http://anijs.github.io/lib/anijs/helpers/dom/anijs-helper-dom.js"></script>
 

    <!-- Placed at the end of the document so the pages load faster -->

    <script src="<?php bloginfo('template_directory') ?>/javascript/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_directory') ?>/javascript/assets/application.js"></script>
  </body>
</html>
