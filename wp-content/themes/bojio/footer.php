 <!-- Subscribe -->
    <section class="subscribe-section" id="section-3">

      <div class="container">

        <div class="row">

          <div class="col-md-12">

            <!-- Title -->
            <h2 id="term"><a href="#">Terms & Privacy</a></h2>
            <p class="line_row"><span><i class="fa fa-mobile-phone"></i></span></p>
            <!-- Subscribe form -->
            <div class="row">

              <div class="col-md-6 col-md-offset-3 col-subscribe">

              
                <section class="subscribe-description">

                  <p>Copyright@2014 Bo Jio now Pvt Ltd</p>
                  <p>Powered by Anovatesoft</p>
                </section> <!-- /.subscribe-description -->
                
              </div> <!-- /.col-md-6 -->

            </div> <!-- /.row -->
            
          </div> <!-- /.col-md-12 -->
          
        </div> <!-- /.row -->

      </div> <!-- /.container -->

    </section> <!-- /.subscribe-section -->


<!--Term and condition Modal-->

<div class="modal smallModalDgn" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header login_head">
            <button type="button" class="close" data-dismiss="modal" >X</button>
            <h4 class="modal-title" id="myModalLabel">Terms & Privacy</h4>
          </div>
          <div class="modal-body">
		<?php //include_once('modal.php'); ?>
		<?php

				$the_query = new WP_Query( 'page_id=43' );
				while($the_query->have_posts()){
				$the_query->the_post();
				  //the_title();
				  the_content();}
				wp_reset_postdata();


				/*$page_idd = 43;
				$page_dataa = get_page( $page_idd ); 
				$ttitle = $page_dataa->post_title; // Get title
				
				$contentt = $page_dataa->post_content; // Get Content
				$sent=apply_filters('post_content', $contentt);
				echo $sent;*/
		?>
	    </div>
	 </div>
	</div>
   </div>
</div>

<script>
$('#term').click(function(){
$('#smallModal').modal('show');
});
</script>
<!--Term and condition Modal ends-->
