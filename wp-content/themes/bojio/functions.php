<?php
/**
  * @category    Mss
 * @package     Mss_Bojio
 * @author      Deepak Mankotia <mss.deepakmankotia@gmail.com>
 */

// Creates Event Custom Post Type
function events_init() {
    $args = array(
      'label' => 'Events',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'events'),
        'query_var' => true,
        'menu_icon' => 'dashicons-video-alt',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes',)
        );
    register_post_type( 'events', $args );
}
	add_theme_support( 'post-thumbnails' );
	add_action( 'init', 'events_init' );
	/*event post ends*/

	add_action('admin_menu', 'my_plugin_menu');

	function my_plugin_menu() {
		add_options_page('Theme Setting', 'Bojio Plugin', 'manage_options', 'functions', 'my_plugin_page');
	}

	function my_plugin_page(){

	if ( !current_user_can( 'manage_options' ) )  {
		  wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	    }
	    
?>
 <div class="wrap">
       <?php screen_icon(); ?><h2>Bojio Theme Settings</h2>
	<?php settings_fields( 'sample_options' ); ?>  

<?php $options = get_option( 'sample_theme_options' ); ?>

        <form method="post" action="">
		
            <?php// wp_nonce_field('update-options') ?>
            <p><strong>Header Title:</strong>
                <input type="text" name="header_title" size="45" value="<?php echo get_option('header_title'); ?>" />
            </p>
		<p><strong>Header Tag line:</strong>
                <input type="text" name="header_tag_line" size="45" value="<?php echo get_option('header_tag_line'); ?>" />
            </p>
		
		<p><strong>Google Play URL:</strong>
                <input type="text" name="google_play_url" size="45" value="<?php echo get_option('google_play_url'); ?>" />
            </p>
		
		<p><strong>App Store:</strong>
                <input type="text" name="app_store" size="45" value="<?php echo get_option('app_store'); ?>" />
            </p>
		
		<p><strong>Header Image:</strong>
                <input type="text" name="header_image" size="45" value="<?php echo get_option('header_image'); ?>" />
            </p>
            <p><input type="submit" name="submit" value="Store Options" /></p>
            <input type="hidden" name="action" value="update" />
            <input type="hidden" name="page_options" value="event_value" />
        </form>
    </div>
<?php

}


if(isset($_POST["submit"])){

    update_option('header_title', $_POST['header_title']);
  
    update_option('header_tag_line', $_POST['header_tag_line']);
	
    update_option('google_play_url', $_POST['google_play_url']);
	
update_option('app_store', $_POST['app_store']);
update_option('header_image', $_POST['header_image']);

     
    echo '<div id="message" class="updated fade"><p>Options Updates</p></div>';
}




?>

