<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Bojio
 * @since Bojio 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<!--custome tage-->
			 <!-- Meta -->
		   	 <meta charset="utf-8">
		    	 <meta name="viewport" content="width=device-width, initial-scale=1.0">
			  <!-- Title -->
		   	 <title>BoJio</title>

		    	<!-- Templates core CSS -->
			  <link href="<?php bloginfo('template_directory') ?>/stylesheets/application.css" rel="stylesheet">
			  <link href="<?php bloginfo('template_directory') ?>/stylesheets/bootstrap.css" rel="stylesheet">
			  <link rel="stylesheet" href="http://anijs.github.io/lib/animationcss/animate.css">
		   
			 <script src="<?php bloginfo('template_directory') ?>/javascript/vendor/jquery-2.1.0.min.js"></script>
			 <script type="text/javascript" src="<?php bloginfo('template_directory') ?>/javascript/scrollview.js"></script>
	<!--custom tage ends-->



	<script>(function(){document.documentElement.className='js'})();</script>
	<?php //wp_head(); ?>
</head>
<body <?php body_class(); ?>>
