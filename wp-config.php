<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'Bojio');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.|Aj8Qatb;-v,qZousu.7J~|X;o8`cj7~Z+-fc3cEd>`_LVlGagp>+1coGW:AH[w');
define('SECURE_AUTH_KEY',  ']u#)*X.>*WpSOu<bJ,tg_D ayo<o+LyFn~!x8~d8-AvX:KV6sQbVno&nR.$}Ryue');
define('LOGGED_IN_KEY',    'eBc{+<k8d{+i nK+ -x3(Az&-kRo-td@`m}+*%-b|~*iCpn/+O$lI*WfO*T&Fn~+');
define('NONCE_KEY',        'zw<L9g?C:Zd|b&qB8ucX}!Aty;#}:z)|*613a@bt`kZ }QJH09|J^d3x3Vg!_RgQ');
define('AUTH_SALT',        '6*TpF<{gE/R@Qm,wPY.w!?[u k+xlc SLfs*6;*-detW[rT}t!w15S=uR=]5?ms/');
define('SECURE_AUTH_SALT', 'KaUq@su=k5m<P?)p^4Ff2x hf@Q[rEz|.FfQ}9x4+g|RZHwceTz~1/-HSM7=+C>r');
define('LOGGED_IN_SALT',   'eZTm1DMUuj_S-y|+U42u18^,{v`;szm*BNAUwbVyo-hgkZ| &3P3=~RYNq`A9s_D');
define('NONCE_SALT',       '[v +NXrv/|yk=4r)HuKs|H3P6|B+$rP|t+GHP,&-W_s f$c<T*s l9r1cKM]SloR');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

